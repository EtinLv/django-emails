# -*- coding: utf-8 -*-
"""
Configuration for a Django app package.

Steps to create a new Django app project:

    mkdir project_name
    cd project_name
    unb template copy-config djpackage

    # Edit the config file copied to project_name/__config__.py

    # Build the template.
    unb template build djpackage

    # Initialize a git repo
    git init

    # Create a project file at ~/.unb-cli.d/projects (see documentation on
    # unb-cli projects for more information on project settings).
    touch ~/.unb-cli.d/projects/project_name

    # Create a virtual environment
    virtualenv venv

    # Initialize the virtual environment
    unb go project_name  # or source venv/bin/activate

    # Install requirements
    pip install -r requirements.txt

    # Perform database migrations
    unb dj migrate  # or ./manage.py migrate

    # Create a superuser account
    # unb dj m createsuperuser  # or ./manage.py createsuperuser

    # Run the server
    unb dj run  # or ./manage.py runserver

"""

from datetime import datetime


# General Project Information
# ===========================

project_name = 'django-emails'
app_name = 'django_emails'
verbose_name = 'Django Emails'

project_description = 'Multiple email management for Django.'

version = '0.0.1'


# Author Information
# ==================

author = 'Nick Zarczynski'
author_email = 'nick@unb.services'


# Links
# =====

# The main url:  A general url that could apply to any audience.
project_url = 'https://bitbucket.org/nickzarr/django-emails/'

# Specific urls
public_url = 'https://bitbucket.org/nickzarr/django-emails/'
source_url = 'https://bitbucket.org/nickzarr/django-emails/'
# docs_url = Found in the Documentation section.
# issue_tracker = Found in the Issue Tracking section.


# License Information
# ===================

def copyright_years(start, end):
  start = str(start)
  end = str(end)
  if start == end:
    return end
  else:
    return start + '-' + end

license = 'MIT'

copyright_start_year = 2015
copyright_end_year = datetime.today().year
copyright_holders = [
  author,
]

mit_copyright_line = ' '.join([
  'Copyright (c)',
  copyright_years(copyright_start_year, copyright_end_year),
  ', '.join(copyright_holders),
])


# Issue Tracking and Contact Information
# ======================================

issue_tracker_url = 'https://bitbucket.org/nickzarr/django-emails/issues'
issue_contact_email = author_email

# URL to direct people that contains information on how to report security
# vulnerabilities found in this software.
#
# Note that it's a very good idea to have this prominently displayed (such as
# in the footer of your site).  You want to make it easy for people to report
# and get acknowledgement of that report, so they don't post it on Twitter!
#
# Lookup "security vulnerability report" to get an idea of how most companies
# handle this.
security_reporting_url = None
security_reporting_email = author_email


primary_mailing_list_address = ''
primary_mailing_list_subscription_url = None


# Documentation
# =============

documentation_url = None


# Virtual Environment
# ===================

# If your virtual environment directory is stored in the project itself, this
# will add it to the .gitignore.  Setting it to `None` will not write anything
# to the .gitignore.
venv_dir = 'venv'


# Django Stuff
# ============

import binascii
import os

secret_key = binascii.hexlify(os.urandom(24))


# Classifiers commonly used in UNB projects
# For a full list: https://pypi.python.org/pypi?%3Aaction=list_classifiers
classifiers = [

  # 'Development Status :: 1 - Planning',
  'Development Status :: 2 - Pre-Alpha',
  # 'Development Status :: 3 - Alpha',
  # 'Development Status :: 4 - Beta',
  # 'Development Status :: 5 - Production/Stable',
  # 'Development Status :: 6 - Mature',
  # 'Development Status :: 7 - Inactive',

  # 'Environment :: Console',
  # 'Environment :: MacOS X',
  'Environment :: Web Environment',
  # 'Environment :: Web Environment :: Mozilla',
  # 'Environment :: X11 Applications',
  # 'Environment :: X11 Applications :: Gnome',
  # 'Environment :: X11 Applications :: GTK',
  # 'Environment :: X11 Applications :: KDE',
  # 'Environment :: X11 Applications :: Qt',

  'Framework :: Django',
  'Framework :: Django :: 1.8',
  # 'Framework :: Flask',
  # 'Framework :: IPython',
  # 'Framework :: Sphinx',

  # 'Intended Audience :: Developers',
  # 'Intended Audience :: End Users/Desktop',

  'License :: OSI Approved :: MIT License',
  # 'License :: Other/Proprietary License',

  'Natural Language :: English',

  # 'Operating System :: Android',
  # 'Operating System :: iOS',
  # 'Operating System :: MacOS :: MacOS X',
  # 'Operating System :: OS Independent',
  'Operating System :: POSIX :: Linux',

  'Programming Language :: Python',
  'Programming Language :: Python :: 2.7',
  # 'Programming Language :: Python :: 2 :: Only',
  # 'Programming Language :: Python :: 3',
  'Programming Language :: JavaScript',
  # 'Programming Language :: Unix Shell',

  # 'Topic :: Documentation :: Sphinx',
  # 'Topic :: Internet',
  # 'Topic :: Internet :: WWW/HTTP :: WSGI',
  'Topic :: Internet :: WWW/HTTP :: WSGI :: Application',
  # 'Topic :: Internet :: WWW/HTTP :: WSGI :: Middleware',
  # 'Topic :: Internet :: WWW/HTTP :: WSGI :: Server',
  # 'Topic :: Software Development :: Build Tools',
  # 'Topic :: Software Development :: Code Generators',
  # 'Topic :: Software Development :: Documentation',
  # 'Topic :: Software Development :: Libraries',
  # 'Topic :: Software Development :: Libraries :: Application Frameworks',
  # 'Topic :: Software Development :: Libraries :: Python Modules',
  # 'Topic :: Software Development :: Testing',
  # 'Topic :: Software Development :: User Interfaces',
  # 'Topic :: Software Development :: Version Control',
  # 'Topic :: System :: Installation/Setup',
  # 'Topic :: Text Editors :: Emacs',
  # 'Topic :: Text Processing :: Markup',
  # 'Topic :: Utilities',
]
