import factory
from factory.django import DjangoModelFactory
from faker import Factory as FakerFactory

from . import models


faker = FakerFactory.create()


class EmailFactory(DjangoModelFactory):
  class Meta:
    model = models.Email

  address = factory.LazyAttribute(lambda x: faker.email())
