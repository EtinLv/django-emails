from django_emails import models as django_emails_models


class Email(django_emails_models.Email):
  class Meta:
    app_label = 'demo'
