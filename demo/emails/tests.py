from datetime import timedelta

from django import db
from django.db import transaction
from django.test import TestCase

from .factories import EmailFactory
from .models import Email


class TestEmail(TestCase):
  # def test_set_primary(self):
  #   e = EmailFactory()

  #   self.assertIsNone(e.primary)

  #   e.make_primary()

  #   self.assertTrue(e.primary)

  def test_archive_non_primary_email(self):
    e = EmailFactory(primary=None)

    self.assertTrue(e.active)
    self.assertIsNone(e.archived_at)

    e.archive()

    e.refresh_from_db()

    self.assertFalse(e.active)
    self.assertIsNotNone(e.archived_at)

  def test_cannot_archive_primary_email(self):
    e = EmailFactory(primary=True)

    self.assertTrue(e.active)
    self.assertIsNone(e.archived_at)

    e.archive()

    e.refresh_from_db()

    self.assertTrue(e.primary)
    self.assertTrue(e.active)
    self.assertIsNone(e.archived_at)

  def test_address_must_not_be_the_empty_string(self):
    with self.assertRaises(db.DataError):
      Email.objects.create(address='')

  def test_confirmation_code(self):
    e = EmailFactory()
    code = e.confirmation_code()

    self.assertTrue(e.validate_confirmation_code(code))

    new_code = e.new_confirmation_code()

    self.assertNotEqual(code, new_code)
    self.assertFalse(e.validate_confirmation_code(code))
    self.assertTrue(e.validate_confirmation_code(new_code))

  def test_add_email_that_is_a_duplicate_of_an_archived_email(self):
    existing_email = EmailFactory(address='foo@bar.com')

    # Adding a duplicate email should fail if the existing email is not
    # archived.
    with self.assertRaises(db.IntegrityError):
      # We have to wrap this in its own transaction otherwise we'd need to
      # explicitly rollback after the failure.
      # https://docs.djangoproject.com/en/dev/topics/db/transactions/#django.db.transaction.atomic
      with transaction.atomic():
        EmailFactory(address='foo@bar.com')

    existing_email.archive()

    # After archiving the existing email, adding a duplicate should succeed.
    EmailFactory(address='foo@bar.com')

  def test_confirmation_timeout(self):
    """Email confirmations older than MAX_CONFIRMATION_AGE should fail."""
    email = EmailFactory(address='foo@bar.com')

    over_max = Email.MAX_CONFIRMATION_AGE + timedelta(seconds=1)
    email.confirmation_sent_at = email.confirmation_sent_at - over_max
    code = email.confirmation_code()

    self.assertFalse(email.validate_confirmation_code(code))
